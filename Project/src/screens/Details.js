import React, {useCallback, useState, useEffect} from 'react';

import {View, Text, StyleSheet, FlatList, ToastAndroid} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Entypo';

import AsyncStorage from '@react-native-async-storage/async-storage';

const Topic = ({name}) => (
  <Text style={{marginTop: 40, fontSize: 20, fontWeight: 'bold'}}>{name}</Text>
);

const MetricLabel = ({title, value}) => (
  <Text style={{fontSize: 15}}>
    - {title}: {value ? value : 'missing'}
  </Text>
);

const LikeIcon = ({name, color, onLike}) => (
  <Icon
    name={name}
    color={color}
    size={35}
    style={{marginRight: 40}}
    onPress={_e => onLike()}
  />
);

export default Details = ({route}) => {
  const {
    crypto: {
      id,
      name,
      isLiked,
      symbol,
      profile: {tagline, relevant_resources: resources},
      metrics: {
        market_data: {price_usd: price, volume_last_24_hours: volume},
        blockchain_stats_24_hours: {
          transaction_volume: transactionVolume,
          count_of_active_addresses: activeAddresses,
          count_of_payments: payments,
          count_of_blocks_added: blocksAdded,
        },
      },
    },
  } = route.params;

  const [firstRender, setFirstRender] = useState(true);
  const [liked, setLiked] = useState(isLiked);

  useEffect(() => {
    if (!firstRender)
      ToastAndroid.show(`${liked ? 'Liked' : 'Unliked'} ${name} crypto!`, 2);
    else setFirstRender(false);
  }, [liked]);

  const handleOnLikePress = () => {
    AsyncStorage.getItem('likedCryptos').then(result => {
      let likedCryptos = result ? JSON.parse(result) : {};

      if (liked) delete likedCryptos[id];
      else likedCryptos[id] = name;

      AsyncStorage.setItem('likedCryptos', JSON.stringify(likedCryptos)).then(
        _res => setLiked(!liked),
      );
    });
  };

  return (
    <SafeAreaView>
      <View style={styles.detailsContainer}>
        <View style={styles.cryptoTitleContainer}>
          <Text style={styles.cryptoTitle}>
            {name} ({symbol})
          </Text>
          {liked && (
            <LikeIcon name="star" color="#f0d829" onLike={handleOnLikePress} />
          )}
          {!liked && (
            <LikeIcon name="star-outlined" onLike={handleOnLikePress} />
          )}
        </View>

        {tagline && <Text style={{fontSize: 15}}>{tagline}</Text>}

        <Topic name="Metrics (last 24 hours)" />
        <View style={{marginTop: 15}}>
          <MetricLabel title="USD Price" value={price} />
          <MetricLabel title="Volume" value={volume} />
          <MetricLabel
            title="(Blockchain) Transaction volume"
            value={transactionVolume}
          />
          <MetricLabel
            title="(Blockchain) Count of active addresses"
            value={activeAddresses}
          />
          <MetricLabel
            title="(Blockchain) Count of payments"
            value={payments}
          />
          <MetricLabel
            title="(Blockchain) Count of blocks added"
            value={blocksAdded}
          />
        </View>

        <Topic name="Relevant resources" />
        <FlatList
          data={resources}
          style={{marginTop: 15}}
          renderItem={({item}) => (
            <Text style={{fontSize: 15}}>{item.url}</Text>
          )}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  detailsContainer: {
    height: '100%',
    paddingTop: 30,
    paddingLeft: 20,
  },

  cryptoTitleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  cryptoTitle: {
    color: 'black',
    fontSize: 30,
    fontWeight: 'bold',
    marginBottom: 8,
  },
});
