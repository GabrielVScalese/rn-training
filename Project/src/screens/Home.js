import React, {useCallback, useEffect, useState} from 'react';

import {
  View,
  Text,
  ActivityIndicator,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  SectionList,
  ToastAndroid,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';

import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {useFocusEffect} from '@react-navigation/native';

export default Home = ({navigation}) => {
  const [cryptos, setCryptos] = useState([]);
  const [cryptoDict, setCryptoDict] = useState({});
  const [loading, setLoading] = useState(true);

  useFocusEffect(
    useCallback(() => {
      axios
        .get('https://data.messari.io/api/v1/assets')
        .then(({data: {data: cryptosData}}) => {
          setCryptos(cryptosData);
          AsyncStorage.getItem('likedCryptos').then(result => {
            JSON.parse(result) && setCryptoDict(JSON.parse(result));
            setLoading(false);
          });
        })
        .catch(_err => {
          ToastAndroid.show('Error on get cryptos', ToastAndroid.SHORT);
        });
    }, []),
  );

  const Item = ({item}) => (
    <TouchableOpacity
      onPress={_e =>
        navigation.navigate('Details', {
          crypto: {...item, isLiked: cryptoDict[item.id] ? true : false},
        })
      }
      style={[
        styles.cryptoContainer,
        {backgroundColor: cryptoDict[item.id] ? 'red' : '#73bf30'},
      ]}>
      <Text style={{fontWeight: 'bold', color: 'black'}}>{item.name}</Text>
    </TouchableOpacity>
  );

  const renderItem = ({item}) => <Item item={item} />;

  return (
    <SafeAreaView>
      <View style={styles.homeContainer}>
        {!loading && (
          <>
            <SectionList
              renderSectionHeader={({section: {title}}) => (
                <Text style={styles.sectionHeader}>{title}</Text>
              )}
              renderSectionFooter={() => (
                <View style={{paddingVertical: 10}}></View>
              )}
              sections={[
                {
                  title: 'Liked cryptos',
                  data: cryptos.filter(c => cryptoDict[c.id]),
                  renderItem,
                },
                {
                  title: 'Other cryptos',
                  data: cryptos.filter(c => !cryptoDict[c.id]),
                  renderItem,
                },
              ]}
              renderItem={renderItem}
              keyExtractor={item => item.id}
              ItemSeparatorComponent={() => (
                <View style={styles.itemSeparator}></View>
              )}></SectionList>
          </>
        )}
        {loading && (
          <ActivityIndicator size="large" color="#53a8e0"></ActivityIndicator>
        )}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  homeContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    paddingTop: 20,
  },

  sectionHeader: {
    fontSize: 20,
    color: 'black',
    fontWeight: 'bold',
    marginBottom: 20,
  },

  itemSeparator: {
    marginTop: 10,
    marginBottom: 10,
    borderWidth: 1,
    borderColor: '#d6d4d4',
  },

  cryptoContainer: {
    width: 350,
    height: 56,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
});
